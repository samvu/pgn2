
# prefixes:
# data - used for storing various misc data
# spr - sprite for player or moby
# txr - texture for a tile
# smap - sprite map to store lots of sprite for easy access in a class
# wndw - refers to something to do with the window or display

# loading status window (add later)
import tkinter as tk
from tkinter import ttk

# create/open save data
import pickle
import os

path = "./pgn2_save.dat"
if os.path.exists(path) != True:
	print("Creating save data")
	gameData = {
		"pigeonHS" : 0,
		"thorntonHS" : 0,
		"waqaHS" : 0,
		"walrusHS" : 0,
		"duckyHS" : 0
	}
	saveFile = open("pgn2_save.dat","wb")
	pickle.dump(gameData,saveFile)
	saveFile.close()
else:
	loadFile = open("pgn2_save.dat","rb")
	gameData = pickle.load(loadFile)
	loadFile.close()
	print("Loaded save data:")
	print(gameData)

scores = []
for i in gameData:
	scores.append(gameData[i])

# modules
import pygame
import sys
from time import sleep
from pygame.locals import *
from random import randint
from math import sin
from math import pi

gibisList = []
skibisList = []

def swallow():
	pass

# initialise pygame
pygame.init()

dataStatus = "menu" # intialise status
dataFrameClock = pygame.time.Clock() # initialise FPS regulator
dataTileDictKey = 0 # finds dictionary key by index (see tileDict)
dataTileFrameCycle = 0 # the current animation cycle for tiles out of 4 (0-3 as indexes)
dataScroll = [2176,0] # camera position
dataCycle = 1 # is the frame odd or even?
dataCycleOsc = 0
dataLevel = 1 # the level the game will display
dataTrailList = [] # this list stores the trail objects

ticker = 0
score = 0
maxJumps = 5
selectedChar = 1
hp = 1

projectileExists = False
projectileCooldown = 0

iFrames = 0

########################################################################


########################################################################


# sprite loading

# selected
sprPigeonSelected = pygame.image.load("images/pigeon select.png")
sprThorntonSelected = pygame.image.load("images/thornton select.png")
sprWaqaSelected = pygame.image.load("images/waqa select.png")
sprWalrusSelected = pygame.image.load("images/walrus select.png")
sprDuckySelected = pygame.image.load("images/ducky select.png")
# pigeon
sprPigeonStillR = pygame.image.load("images/pigeonStanding.png")
sprPigeonStillL = pygame.transform.flip(sprPigeonStillR, True, False)
sprPigeonWalk1R = pygame.image.load("images/pigeonWalk1.png")
sprPigeonWalk1L = pygame.transform.flip(sprPigeonWalk1R, True, False)
sprPigeonWalk2R = pygame.image.load("images/pigeonWalk2.png")
sprPigeonWalk2L = pygame.transform.flip(sprPigeonWalk2R, True, False)
sprPigeonFly1R = pygame.image.load("images/pigeonFly1.png")
sprPigeonFly1L = pygame.transform.flip(sprPigeonFly1R, True, False)
sprPigeonFly2R = pygame.image.load("images/pigeonFly2.png")
sprPigeonFly2L = pygame.transform.flip(sprPigeonFly2R, True, False)
#ibis
sprIbisStillR = pygame.image.load("images/ibisStanding.png")
sprIbisStillL = pygame.transform.flip(sprIbisStillR, True, False)
sprIbisWalk1R = pygame.image.load("images/ibisWalk1.png")
sprIbisWalk1L = pygame.transform.flip(sprIbisWalk1R, True, False)
sprIbisWalk2R = pygame.image.load("images/ibisWalk2.png")
sprIbisWalk2L = pygame.transform.flip(sprIbisWalk2R, True, False)
sprIbisFly1R = pygame.image.load("images/ibisFly1.png")
sprIbisFly1L = pygame.transform.flip(sprIbisFly1R, True, False)
sprIbisFly2R = pygame.image.load("images/ibisFly2.png")
sprIbisFly2L = pygame.transform.flip(sprIbisFly2R, True, False)
# thornton
sprThorntonStillR = pygame.image.load("images/thornton standing.png")
sprThorntonStillL = pygame.transform.flip(sprThorntonStillR, True, False)
sprThorntonWalk1R = pygame.image.load("images/thornton walk 1.png")
sprThorntonWalk1L = pygame.transform.flip(sprThorntonWalk1R, True, False)
sprThorntonWalk2R = pygame.image.load("images/thornton walk 2.png")
sprThorntonWalk2L = pygame.transform.flip(sprThorntonWalk2R, True, False)
sprThorntonJumpR = pygame.image.load("images/thornton jump.png")
sprThorntonJumpL = pygame.transform.flip(sprThorntonJumpR, True, False)
sprThorntonFallR = pygame.image.load("images/thornton fall.png")
sprThorntonFallL = pygame.transform.flip(sprThorntonFallR, True, False)
# waqa
sprWaqaStillR = pygame.image.load("images/waqa standing.png")
sprWaqaStillL = pygame.transform.flip(sprWaqaStillR, True, False)
sprWaqaWalk1R = pygame.image.load("images/waqa walk 1.png")
sprWaqaWalk1L = pygame.transform.flip(sprWaqaWalk1R, True, False)
sprWaqaWalk2R = pygame.image.load("images/waqa walk 2.png")
sprWaqaWalk2L = pygame.transform.flip(sprWaqaWalk2R, True, False)
sprWaqaJumpR = pygame.image.load("images/waqa jump.png")
sprWaqaJumpL = pygame.transform.flip(sprWaqaJumpR, True, False)
sprWaqaFallR = pygame.image.load("images/waqa fall.png")
sprWaqaFallL = pygame.transform.flip(sprWaqaFallR, True, False)
# walrus
sprWalrusStillR = pygame.image.load("images/walrus standing.png")
sprWalrusStillL = pygame.transform.flip(sprWalrusStillR, True, False)
sprWalrusWalk1R = pygame.image.load("images/walrus walk 1.png")
sprWalrusWalk1L = pygame.transform.flip(sprWalrusWalk1R, True, False)
sprWalrusWalk2R = pygame.image.load("images/walrus walk 2.png")
sprWalrusWalk2L = pygame.transform.flip(sprWalrusWalk2R, True, False)
sprWalrusJumpR = pygame.image.load("images/walrus jump.png")
sprWalrusJumpL = pygame.transform.flip(sprWalrusJumpR, True, False)
# duck
sprDuckyR = pygame.image.load("images/ducky.png")
sprDuckyL = pygame.transform.flip(sprDuckyR, True, False)
# pigeon trail
sprPigeonStillRt = pygame.image.load("images/pigeonStanding.png")
sprPigeonStillLt = pygame.transform.flip(sprPigeonStillRt, True, False)
sprPigeonWalk1Rt = pygame.image.load("images/pigeonWalk1.png")
sprPigeonWalk1Lt = pygame.transform.flip(sprPigeonWalk1Rt, True, False)
sprPigeonWalk2Rt = pygame.image.load("images/pigeonWalk2.png")
sprPigeonWalk2Lt = pygame.transform.flip(sprPigeonWalk2Rt, True, False)
sprPigeonFly1Rt = pygame.image.load("images/pigeonFly1.png")
sprPigeonFly1Lt = pygame.transform.flip(sprPigeonFly1Rt, True, False)
sprPigeonFly2Rt = pygame.image.load("images/pigeonFly2.png")
sprPigeonFly2Lt = pygame.transform.flip(sprPigeonFly2Rt, True, False)
sprPigeonStillRt.set_alpha(100)
sprPigeonStillLt.set_alpha(100)
sprPigeonWalk1Rt.set_alpha(100)
sprPigeonWalk1Lt.set_alpha(100)
sprPigeonWalk2Rt.set_alpha(100)
sprPigeonWalk2Lt.set_alpha(100)
sprPigeonFly1Rt.set_alpha(100)
sprPigeonFly1Lt.set_alpha(100)
sprPigeonFly2Rt.set_alpha(100)
sprPigeonFly2Lt.set_alpha(100)
# thornton trail
sprThorntonStillRt = pygame.image.load("images/thornton standing.png")
sprThorntonStillLt = pygame.transform.flip(sprThorntonStillR, True, False)
sprThorntonWalk1Rt = pygame.image.load("images/thornton walk 1.png")
sprThorntonWalk1Lt = pygame.transform.flip(sprThorntonWalk1R, True, False)
sprThorntonWalk2Rt = pygame.image.load("images/thornton walk 2.png")
sprThorntonWalk2Lt = pygame.transform.flip(sprThorntonWalk2R, True, False)
sprThorntonJumpRt = pygame.image.load("images/thornton jump.png")
sprThorntonJumpLt = pygame.transform.flip(sprThorntonJumpR, True, False)
sprThorntonFallRt = pygame.image.load("images/thornton fall.png")
sprThorntonFallLt = pygame.transform.flip(sprThorntonFallR, True, False)
sprThorntonStillRt.set_alpha(100)
sprThorntonStillLt.set_alpha(100)
sprThorntonWalk1Rt.set_alpha(100)
sprThorntonWalk1Lt.set_alpha(100)
sprThorntonWalk2Rt.set_alpha(100)
sprThorntonWalk2Lt.set_alpha(100)
sprThorntonJumpRt.set_alpha(100)
sprThorntonJumpLt.set_alpha(100)
sprThorntonFallRt.set_alpha(100)
sprThorntonFallLt.set_alpha(100)
# waqa trail
sprWaqaStillRt = pygame.image.load("images/waqa standing.png")
sprWaqaStillLt = pygame.transform.flip(sprWaqaStillR, True, False)
sprWaqaWalk1Rt = pygame.image.load("images/waqa walk 1.png")
sprWaqaWalk1Lt = pygame.transform.flip(sprWaqaWalk1R, True, False)
sprWaqaWalk2Rt = pygame.image.load("images/waqa walk 2.png")
sprWaqaWalk2Lt = pygame.transform.flip(sprWaqaWalk2R, True, False)
sprWaqaJumpRt = pygame.image.load("images/waqa jump.png")
sprWaqaJumpLt = pygame.transform.flip(sprWaqaJumpR, True, False)
sprWaqaFallRt = pygame.image.load("images/waqa fall.png")
sprWaqaFallLt = pygame.transform.flip(sprWaqaFallR, True, False)
sprWaqaStillRt.set_alpha(100)
sprWaqaStillLt.set_alpha(100)
sprWaqaWalk1Rt.set_alpha(100)
sprWaqaWalk1Lt.set_alpha(100)
sprWaqaWalk2Rt.set_alpha(100)
sprWaqaWalk2Lt.set_alpha(100)
sprWaqaJumpRt.set_alpha(100)
sprWaqaJumpLt.set_alpha(100)
sprWaqaFallRt.set_alpha(100)
sprWaqaFallLt.set_alpha(100)
# walrus trail
sprWalrusStillRt = pygame.image.load("images/walrus standing.png")
sprWalrusStillLt = pygame.transform.flip(sprWalrusStillR, True, False)
sprWalrusWalk1Rt = pygame.image.load("images/walrus walk 1.png")
sprWalrusWalk1Lt = pygame.transform.flip(sprWalrusWalk1R, True, False)
sprWalrusWalk2Rt = pygame.image.load("images/walrus walk 2.png")
sprWalrusWalk2Lt = pygame.transform.flip(sprWalrusWalk2R, True, False)
sprWalrusJumpRt = pygame.image.load("images/walrus jump.png")
sprWalrusJumpLt = pygame.transform.flip(sprWalrusJumpR, True, False)
sprWalrusStillRt.set_alpha(100)
sprWalrusStillLt.set_alpha(100)
sprWalrusWalk1Rt.set_alpha(100)
sprWalrusWalk1Lt.set_alpha(100)
sprWalrusWalk2Rt.set_alpha(100)
sprWalrusWalk2Lt.set_alpha(100)
sprWalrusJumpRt.set_alpha(100)
sprWalrusJumpLt.set_alpha(100)
# duck trail
sprDuckyRt = pygame.image.load("images/ducky.png")
sprDuckyLt = pygame.transform.flip(sprDuckyR, True, False)
sprDuckyRt.set_alpha(100)
sprDuckyLt.set_alpha(100)

sprHat = pygame.image.load("images/hat.png")

# level 1 tiles
txrGrassFlat = pygame.image.load("images/grassFlat.png")
txrDirt = pygame.image.load("images/dirt.png")
txrGrassCorner = pygame.image.load("images/grassCorner.png")
txrGrassEdgeR = pygame.image.load("images/grassEdgeR.png")
txrGrassEdgeL = pygame.image.load("images/grassEdgeL.png")
txrGrassSide = pygame.image.load("images/grassSide.png")
txrStonePlatform = pygame.image.load("images/stonePlatform.png")
txrGrassDecor1 = pygame.image.load("images/grassDecor1.png")
txrGrassDecor2 = pygame.image.load("images/grassDecor2.png")
txrGrassDecor3 = pygame.image.load("images/grassDecor3.png")
txrGrassDecor4 = pygame.image.load("images/grassDecor4.png")
txrStalactite1 = pygame.image.load("images/stalactite1.png")
txrStalactite2 = pygame.image.load("images/stalactite2.png")
txrStalactite3 = pygame.image.load("images/stalactite3.png")
txrVine1 = pygame.image.load("images/vineSmall.png")
txrVine2 = pygame.image.load("images/vineMedium.png")
txrVine3 = pygame.image.load("images/vineBig.png")
txrChainL = pygame.image.load("images/chainL.png")
txrChainR = pygame.image.load("images/chainR.png")
txrPlatform = pygame.image.load("images/stonePlatform.png")



# level 2 tiles
txrCaveStone = pygame.image.load("images/caveStone.png")
txrDirtStone = pygame.image.load("images/caveDirt.png")
txrStoneStalactite1 = pygame.image.load("images/caveStalagmite1.png")
txrStoneStalactite2 = pygame.image.load("images/caveStalagmite2.png")
txrStoneStalactite3 = pygame.image.load("images/caveStalagmite3.png")
txrStoneStalactite4 = pygame.image.load("images/caveStalagmite4.png")
txrDirtStalactite1 = pygame.image.load("images/dirtStalagmite1.png")
txrDirtStalactite2 = pygame.image.load("images/dirtStalagmite2.png")
txrDirtStalactite3 = pygame.image.load("images/dirtStalagmite3.png")
txrDirtStalactite4 = pygame.image.load("images/dirtStalagmite4.png")
txrOre1 = pygame.image.load("images/ore1.png")
txrOre2 = pygame.image.load("images/ore2.png")
txrOre3 = pygame.image.load("images/ore3.png")
txrOre4 = pygame.image.load("images/ore4.png")
txrPillarBottom = pygame.image.load("images/pillarBottom.png")
txrPillarMiddle = pygame.image.load("images/pillarMiddle.png")
txrPillarTop = pygame.image.load("images/pillarTop.png")
txrSupportLeft = pygame.image.load("images/supportLeft.png")
txrSupportRight = pygame.image.load("images/supportRight.png")
txrPillarBottomRev = pygame.transform.flip(txrPillarBottom, True, False)
txrPillarMiddleRev = pygame.transform.flip(txrPillarMiddle, True, False)
txrPillarTopRev = pygame.transform.flip(txrPillarTop, True, False)
txrSupportLeftRev = pygame.transform.flip(txrSupportLeft, True, False)
txrSupportRightRev = pygame.transform.flip(txrSupportRight, True, False)
txrUnder = pygame.image.load("images/undergroundBackground.png")
txrTransition1 = pygame.image.load("images/transition1.png")
txrTransition2 = pygame.image.load("images/transition2.png")
txrTransition3 = pygame.image.load("images/transition3.png")
txrTransition4 = pygame.image.load("images/transition4.png")

########################################################################


########################################################################

# tile map (access like "map[tileY][tileX]")
tileX = 0
tileY = 0
tileHitboxes = [] # stores locations of tile hitboxes
liquidHitboxes = [] # stores locations of liquid hitboxes
# coords example = [
#               [y0x0, y0x1, y0x2],
#               [y1x0, y1x1, y1x2]]
mapLevel1 = [
	#   [0,   1,   2,   3,   4,   5,   6,   7,   8,   9,   10,  11,  12,  13,  14,  15,  16,  17,  18,  19,  20,  21,  22,  23,  24,  25,  26,  27,  28,  29,  30,  31,  32,  33]
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 0
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 1
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 2
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 3
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 4
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 5
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 6
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 7
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 8
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 9
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 10
		["00","00","00","00","13","00","00","00","00","00","00","14","00","00","00","15","00","00","16","00","00","00","13","00","00","00","00","00","00","14","00","00","00","00"], # 11
		["00","00","00","00","01","00","00","00","00","00","00","01","00","00","00","01","00","00","01","00","00","00","01","00","00","00","00","00","00","01","00","00","00","00"], # 12
		["00","00","00","00","17","00","00","00","00","00","00","18","00","00","00","19","00","00","18","00","00","00","17","00","00","00","00","00","00","19","00","00","00","00"], # 13
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 14
		["00","00","00","00","00","13","16","14","13","15","16","14","15","13","16","14","13","15","14","13","16","14","16","13","15","14","16","14","13","00","00","00","00","00"], # 15
		["00","00","00","00","00","01","01","01","01","01","01","01","01","01","01","01","01","01","01","01","01","01","01","01","01","01","01","01","01","00","00","00","00","00"], # 16
		["00","00","00","00","00","20","02","02","02","02","02","02","02","02","02","02","02","02","02","02","02","02","02","02","02","02","02","02","20","00","00","00","00","00"], # 17
		["00","00","00","00","00","00","21","17","00","21","18","19","00","21","20","18","21","20","19","00","20","17","21","20","19","00","20","18","00","00","00","00","00","00"], # 18
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"], # 19
		["00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00","00"] # 20
	]



# tile dictionary
# number at end of lists denote the type of hitbox (0 for solids, 1 for liquids, 2 for none)
# ADD IN FUTURE:
#   (could also add another item in list with tile hitbox/dimensions)
tileDict = {
		"01"  : [[txrGrassFlat,txrGrassFlat,txrGrassFlat,txrGrassFlat], 0], # flat grass
		"02"  : [[txrDirt,txrDirt,txrDirt,txrDirt], 0], # dirt
		"03"  : [[txrGrassCorner,txrGrassCorner,txrGrassCorner,txrGrassCorner], 0], # grass corner
		"04"  : [[txrGrassEdgeR,txrGrassEdgeR,txrGrassEdgeR,txrGrassEdgeR], 0], # right grass edge
		"05"  : [[txrGrassEdgeL,txrGrassEdgeL,txrGrassEdgeL,txrGrassEdgeL], 0], # left grass edge
		"06"  : [[txrGrassSide,txrGrassSide,txrGrassSide,txrGrassSide], 0], # right grass side
		"07"  : [[txrStonePlatform,txrStonePlatform,txrStonePlatform,txrStonePlatform], 0], # stone platform
		"13"  : [[txrGrassDecor1,txrGrassDecor1,txrGrassDecor1,txrGrassDecor1], 2], # grass blades 1
		"14"  : [[txrGrassDecor2,txrGrassDecor2,txrGrassDecor2,txrGrassDecor2], 2], # grass blades 2
		"15"  : [[txrGrassDecor3,txrGrassDecor3,txrGrassDecor3,txrGrassDecor3], 2], # grass blades 3
		"16"  : [[txrGrassDecor4,txrGrassDecor4,txrGrassDecor4,txrGrassDecor4], 2], # grass blades 4
		"17"  : [[txrStalactite1,txrStalactite1,txrStalactite1,txrStalactite1], 2], # stalactite 1
		"18"  : [[txrStalactite2,txrStalactite2,txrStalactite2,txrStalactite2], 2], # stalactite 2
		"19"  : [[txrStalactite3,txrStalactite3,txrStalactite3,txrStalactite3], 2], # stalactite 3
		"20"  : [[txrVine1,txrVine1,txrVine1,txrVine1], 2], # vine 1
		"21"  : [[txrVine2,txrVine2,txrVine2,txrVine2], 2], # vine 2
		"22"  : [[txrVine3,txrVine3,txrVine3,txrVine3], 2], # vine 3
		"23"  : [[txrDirt,txrDirt,txrDirt,txrDirt], 0],
		"24"  : [[txrChainL,txrChainL,txrChainL,txrChainL], 3],
		"25"  : [[txrChainR,txrChainR,txrChainR,txrChainR], 3],
		"26"  : [[txrPlatform,txrPlatform,txrPlatform,txrPlatform], 0],
		"27"  : [[txrCaveStone,txrCaveStone,txrCaveStone,txrCaveStone], 0],
		"28"  : [[txrDirtStone,txrDirtStone,txrDirtStone,txrDirtStone], 0],
		"29"  : [[txrStoneStalactite1,txrStoneStalactite1,txrStoneStalactite1,txrStoneStalactite1], 0],
		"30"  : [[txrStoneStalactite2,txrStoneStalactite2,txrStoneStalactite2,txrStoneStalactite2], 0],
		"31"  : [[txrStoneStalactite3,txrStoneStalactite3,txrStoneStalactite3,txrStoneStalactite3], 0],
		"32"  : [[txrStoneStalactite4,txrStoneStalactite4,txrStoneStalactite4,txrStoneStalactite4], 0],
		"33"  : [[txrDirtStalactite1,txrDirtStalactite1,txrDirtStalactite1,txrDirtStalactite1], 2],
		"34"  : [[txrDirtStalactite2,txrDirtStalactite2,txrDirtStalactite2,txrDirtStalactite2], 2],
		"35"  : [[txrDirtStalactite3,txrDirtStalactite3,txrDirtStalactite3,txrDirtStalactite3], 2],
		"36"  : [[txrDirtStalactite4,txrDirtStalactite4,txrDirtStalactite4,txrDirtStalactite4], 2],
		"37"  : [[txrOre1,txrOre1,txrOre1,txrOre1], 0],
		"38"  : [[txrOre2,txrOre2,txrOre2,txrOre2], 0],
		"39"  : [[txrOre3,txrOre3,txrOre3,txrOre3], 0],
		"40"  : [[txrOre4,txrOre4,txrOre4,txrOre4], 0],
		"41"  : [[txrPillarBottom,txrPillarBottom,txrPillarBottom,txrPillarBottom], 3],
		"42"  : [[txrPillarMiddle,txrPillarMiddle,txrPillarMiddle,txrPillarMiddle], 3],
		"43"  : [[txrPillarTop,txrPillarTop,txrPillarTop,txrPillarTop], 3],
		"44"  : [[txrSupportLeft,txrSupportLeft,txrSupportLeft,txrSupportLeft], 3],
		"45"  : [[txrSupportRight,txrSupportRight,txrSupportRight,txrSupportRight], 3],
		"46"  : [[txrPillarBottomRev,txrPillarBottomRev,txrPillarBottomRev,txrPillarBottomRev], 3],
		"47"  : [[txrPillarMiddleRev,txrPillarMiddleRev,txrPillarMiddleRev,txrPillarMiddleRev], 3],
		"48"  : [[txrPillarTopRev,txrPillarTopRev,txrPillarTopRev,txrPillarTopRev], 3],
		"49"  : [[txrSupportLeftRev,txrSupportLeftRev,txrSupportLeftRev,txrSupportLeftRev], 3],
		"50"  : [[txrSupportRightRev,txrSupportRightRev,txrSupportRightRev,txrSupportRightRev], 3],
		"51"  : [[txrUnder,txrUnder,txrUnder,txrUnder], 2],
		"52"  : [[txrTransition1,txrTransition1,txrTransition1,txrTransition1], 0],
		"53"  : [[txrTransition2,txrTransition2,txrTransition2,txrTransition2], 0],
		"54"  : [[txrTransition3,txrTransition3,txrTransition3,txrTransition3], 0],
		"55"  : [[txrTransition4,txrTransition4,txrTransition4,txrTransition4], 0]
		}
tileDictList = list(tileDict) # this is to access tileDict's keys by index

########################################################################


########################################################################

# sprite map creation
smapPigeon = {
		"sr" : [sprPigeonStillR,sprPigeonStillRt],
		"sl" : [sprPigeonStillL,sprPigeonStillLt],
		"jr" : [sprPigeonFly2R,sprPigeonFly2Rt],
		"jl" : [sprPigeonFly2L,sprPigeonFly2Lt],
		"fr" : [sprPigeonFly1R,sprPigeonFly1Rt],
		"fl" : [sprPigeonFly1L,sprPigeonFly1Lt],
		"wr1": [sprPigeonWalk1R,sprPigeonWalk1Rt],
		"wl1": [sprPigeonWalk1L,sprPigeonWalk1Lt],
		"wr2": [sprPigeonWalk2R,sprPigeonWalk2Rt],
		"wl2": [sprPigeonWalk2L,sprPigeonWalk2Lt]
}

smapIbis = {
		"sr" : [sprIbisStillR,sprPigeonStillRt],
		"sl" : [sprIbisStillL,sprPigeonStillLt],
		"jr" : [sprIbisFly2R,sprPigeonFly2Rt],
		"jl" : [sprIbisFly2L,sprPigeonFly2Lt],
		"fr" : [sprIbisFly1R,sprPigeonFly1Rt],
		"fl" : [sprIbisFly1L,sprPigeonFly1Lt],
		"wr1": [sprIbisWalk1R,sprPigeonWalk1Rt],
		"wl1": [sprIbisWalk1L,sprPigeonWalk1Lt],
		"wr2": [sprIbisWalk2R,sprPigeonWalk2Rt],
		"wl2": [sprIbisWalk2L,sprPigeonWalk2Lt]
}

smapThornton = {
		"sr" : [sprThorntonStillR,sprThorntonStillRt],
		"sl" : [sprThorntonStillL,sprThorntonStillLt],
		"jr" : [sprThorntonJumpR,sprThorntonJumpRt],
		"jl" : [sprThorntonJumpL,sprThorntonJumpLt],
		"fr" : [sprThorntonFallR,sprThorntonFallRt],
		"fl" : [sprThorntonFallL,sprThorntonFallLt],
		"wr1": [sprThorntonWalk1R,sprThorntonWalk1Rt],
		"wl1": [sprThorntonWalk1L,sprThorntonWalk1Lt],
		"wr2": [sprThorntonWalk2R,sprThorntonWalk2Rt],
		"wl2": [sprThorntonWalk2L,sprThorntonWalk2Lt]
}

smapWaqa = {
		"sr" : [sprWaqaStillR,sprWaqaStillRt],
		"sl" : [sprWaqaStillL,sprWaqaStillLt],
		"jr" : [sprWaqaJumpR,sprWaqaJumpRt],
		"jl" : [sprWaqaJumpL,sprWaqaJumpLt],
		"fr" : [sprWaqaFallR,sprWaqaFallRt],
		"fl" : [sprWaqaFallL,sprWaqaFallLt],
		"wr1": [sprWaqaWalk1R,sprWaqaWalk1Rt],
		"wl1": [sprWaqaWalk1L,sprWaqaWalk1Lt],
		"wr2": [sprWaqaWalk2R,sprWaqaWalk2Rt],
		"wl2": [sprWaqaWalk2L,sprWaqaWalk2Lt]
}

smapWalrus = {
		"sr" : [sprWalrusStillR,sprWalrusStillRt],
		"sl" : [sprWalrusStillL,sprWalrusStillLt],
		"jr" : [sprWalrusJumpR,sprWalrusJumpRt],
		"jl" : [sprWalrusJumpL,sprWalrusJumpLt],
		"fr" : [sprWalrusJumpR,sprWalrusJumpRt],
		"fl" : [sprWalrusJumpL,sprWalrusJumpLt],
		"wr1": [sprWalrusWalk1R,sprWalrusWalk1Rt],
		"wl1": [sprWalrusWalk1L,sprWalrusWalk1Lt],
		"wr2": [sprWalrusWalk2R,sprWalrusWalk2Rt],
		"wl2": [sprWalrusWalk2L,sprWalrusWalk2Lt]
}

smapDucky = {
		"sr" : [sprDuckyR,sprDuckyRt],
		"sl" : [sprDuckyL,sprDuckyLt],
		"jr" : [sprDuckyR,sprDuckyRt],
		"jl" : [sprDuckyL,sprDuckyLt],
		"fr" : [sprDuckyR,sprDuckyRt],
		"fl" : [sprDuckyL,sprDuckyLt],
		"wr1": [sprDuckyR,sprDuckyRt],
		"wl1": [sprDuckyL,sprDuckyLt],
		"wr2": [sprDuckyR,sprDuckyRt],
		"wl2": [sprDuckyL,sprDuckyLt]
}

smaps = [smapPigeon,smapThornton,smapWaqa,smapWalrus,smapDucky]
xSpeeds = [9,5,3,2,15]
ySpeeds = [-20,-19,-23,-25,-32]
jumps = [5,3,2,2,1]
dashDuration = [10,7,0,4,10]

########################################################################








########################################################################

# classes for gameplay
class Thing:
	def __init__(self, x, y, sprList, spr, hitbox):
		self.x = x
		self.y = y
		self.sprList = sprList
		self.spr = self.sprList["sr"][0]
		self.hitbox = self.hitbox = pygame.Rect(self.x,self.y,self.spr.get_width(),self.spr.get_height())
	def hitboxUpdate(self):
		# updates the hitbox x and y to the object's x and y
		self.hitbox = pygame.Rect(self.x,self.y,sprPigeonStillR.get_width(),sprPigeonStillR.get_height())
	def blit(self):
		# displays the object's sprite
		wndwScreen.blit(self.spr, (self.x-dataScroll[0],self.y-dataScroll[1]))

########################################################################




class Hat:
	
	global sprHat
	

	direction = ""
	
	def __init__(self, x, y, hitbox):
		
		global direction
		self.x = x
		self.y = y
		self.hitbox = pygame.Rect(self.x,self.y,sprHat.get_width(),sprHat.get_height())
		
		if iFrames == 0:
			opacity = 0
		else:
			opacity = 1
		if player.spr == player.sprList["sr"][opacity] or player.spr == player.sprList["jr"][opacity] or player.spr == player.sprList["fr"][opacity] or player.spr == player.sprList["wr1"][opacity] or player.spr == player.sprList["wr2"][opacity]:
			direction = "right"
		else:
			direction = "left"
	def hitboxUpdate(self):
		
		self.hitbox = pygame.Rect(self.x,self.y,sprHat.get_width(),sprHat.get_height())
	def blit(self):
		
		wndwScreen.blit(sprHat, (self.x-dataScroll[0],self.y-dataScroll[1]))
		
	def motion(self):
		global direction
		if direction == "right":
			self.x += 35
		else:
			self.x -= 35



########################################################################

class Player(Thing):
	
	def __init__(self, x, y, grav, sprList, spr, movingLeft, movingRight, hitbox, inLiquid, jumps, dash):
		super().__init__(x, y, sprList, spr, hitbox)
		self.movingLeft = False
		self.movingRight = False
		self.grav = 0
		self.inLiquid = False
		self.jumps = 0
		self.dash = False
	
	
	
	
	
	
	
	
	
	
	
	# animations
		
	def sprSet(self):
		
		global animatedCheck
		global iFrames
		
		if iFrames == 0:
			opacity = 0
		else:
			opacity = 1
		
		if self.grav == 0:
			self.jumps = 0
		elif self.grav > 1:
			if self.movingRight:
				self.spr = self.sprList["fr"][opacity]
			elif self.movingLeft:
				self.spr = self.sprList["fl"][opacity]
			elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
				self.spr = self.sprList["fr"][opacity]
			else:
				self.spr = self.sprList["fl"][opacity]
		
		if self.jumps == 0 and (self.grav == 0 or self.grav == 0.95):
			if self.movingRight:
				if dataCycle <= 5:
					self.spr = self.sprList["wr1"][opacity]
				else:
					self.spr = self.sprList["wr2"][opacity]
			elif self.movingLeft:
				if dataCycle <= 5:
					self.spr = self.sprList["wl1"][opacity]
				else:
					self.spr = self.sprList["wl2"][opacity]
			elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][0] or self.spr == self.sprList["fr"][0] or self.spr == self.sprList["wr1"][0] or self.spr == self.sprList["wr2"][0]:
				self.spr = self.sprList["sr"][opacity]
			else:
				self.spr = self.sprList["sl"][opacity]
		
		if self.jumps == 1:
			if self.grav < 0:
				if self.movingRight:
					self.spr = self.sprList["jr"][opacity]
				elif self.movingLeft:
					self.spr = self.sprList["jl"][opacity]
				elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
					self.spr = self.sprList["jr"][opacity]
				else:
					self.spr = self.sprList["jl"][opacity]
			else:
				if self.movingRight:
					self.spr = self.sprList["fr"][opacity]
				elif self.movingLeft:
					self.spr = self.sprList["fl"][opacity]
				elif self.spr == self.sprList["sr"] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
					self.spr = self.sprList["fr"][opacity]
				else:
					self.spr = self.sprList["fl"][opacity]
		
		if self.jumps == 2:
			if self.grav < 0:
				if animatedCheck == False:
					if self.spr == self.sprList["jl"][opacity] or self.spr == self.sprList["jr"][opacity]:
						if self.movingRight:
							self.spr = self.sprList["fr"][opacity]
						elif self.movingLeft:
							self.spr = self.sprList["fl"][opacity]
						elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
							self.spr = self.sprList["fr"][opacity]
						else:
							self.spr = self.sprList["fl"][opacity]
					else: 
						if self.movingRight:
							self.spr = self.sprList["jr"][opacity]
						elif self.movingLeft:
							self.spr = self.sprList["jl"][opacity]
						elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
							self.spr = self.sprList["jr"][opacity]
						else:
							self.spr = self.sprList["jl"][opacity]
					animatedCheck = True
			else:
				if self.movingRight:
					self.spr = self.sprList["fr"][opacity]
				elif self.movingLeft:
					self.spr = self.sprList["fl"][opacity]
				elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
					self.spr = self.sprList["fr"][opacity]
				else:
					self.spr = self.sprList["fl"][opacity]
					
		if self.jumps == 3:
			if self.grav < 0:
				if animatedCheck == False:
					if self.spr == self.sprList["jl"][opacity] or self.spr == self.sprList["jr"][opacity]:
						if self.movingRight:
							self.spr = self.sprList["fr"][opacity]
						elif self.movingLeft:
							self.spr = self.sprList["fl"][opacity]
						elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
							self.spr = self.sprList["fr"][opacity]
						else:
							self.spr = self.sprList["fl"][opacity]
					else: 
						if self.movingRight:
							self.spr = self.sprList["jr"][opacity]
						elif self.movingLeft:
							self.spr = self.sprList["jl"][opacity]
						elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
							self.spr = self.sprList["jr"][opacity]
						else:
							self.spr = self.sprList["jl"][opacity]
					animatedCheck = True
			else:
				if self.movingRight:
					self.spr = self.sprList["fr"][opacity]
				elif self.movingLeft:
					self.spr = self.sprList["fl"][opacity]
				elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
					self.spr = self.sprList["fr"][opacity]
				else:
					self.spr = self.sprList["fl"][opacity]
	
		if self.jumps == 4:
			if self.grav < 0:
				if animatedCheck == False:
					if self.spr == self.sprList["jl"][opacity] or self.spr == self.sprList["jr"][opacity]:
						if self.movingRight:
							self.spr = self.sprList["fr"][opacity]
						elif self.movingLeft:
							self.spr = self.sprList["fl"][opacity]
						elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
							self.spr = self.sprList["fr"][opacity]
						else:
							self.spr = self.sprList["fl"][opacity]
					else: 
						if self.movingRight:
							self.spr = self.sprList["jr"][opacity]
						elif self.movingLeft:
							self.spr = self.sprList["jl"][opacity]
						elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
							self.spr = self.sprList["jr"][opacity]
						else:
							self.spr = self.sprList["jl"][opacity]
					animatedCheck = True
			else:
				if self.movingRight:
					self.spr = self.sprList["fr"][opacity]
				elif self.movingLeft:
					self.spr = self.sprList["fl"][opacity]
				elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
					self.spr = self.sprList["fr"][opacity]
				else:
					self.spr = self.sprList["fl"][opacity]
		
		if self.jumps == 5:
			if self.grav < 0:
				if animatedCheck == False:
					if self.spr == self.sprList["jl"][opacity] or self.spr == self.sprList["jr"][opacity]:
						if self.movingRight:
							self.spr = self.sprList["fr"][opacity]
						elif self.movingLeft:
							self.spr = self.sprList["fl"][opacity]
						elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
							self.spr = self.sprList["fr"][opacity]
						else:
							self.spr = self.sprList["fl"][opacity]
					else: 
						if self.movingRight:
							self.spr = self.sprList["jr"][opacity]
						elif self.movingLeft:
							self.spr = self.sprList["jl"][opacity]
						elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
							self.spr = self.sprList["jr"][opacity]
						else:
							self.spr = self.sprList["jl"][opacity]
					animatedCheck = True
			else:
				if self.movingRight:
					self.spr = self.sprList["fr"][opacity]
				elif self.movingLeft:
					self.spr = self.sprList["fl"][opacity]
				elif self.spr == self.sprList["sr"][opacity] or self.spr == self.sprList["jr"][opacity] or self.spr == self.sprList["fr"][opacity] or self.spr == self.sprList["wr1"][opacity] or self.spr == self.sprList["wr2"][opacity]:
					self.spr = self.sprList["fr"][opacity]
				else:
					self.spr = self.sprList["fl"][opacity]
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
	def movementX(self):
		
		# left and right movement
		if self.movingLeft == True:
			if self.inLiquid == True:
				self.x -= 1
			else:
				if self.dash:
					self.x -= 50
				else:
					self.x -= xSpeeds[selectedChar-1]
		if self.movingRight == True:
			if self.inLiquid == True:
				self.x += 1
			else:
				if self.dash:
					self.x += 50
				else:
					self.x += xSpeeds[selectedChar-1]
		
		collisionsX()
		
		self.hitboxUpdate()
		
	def movementY(self):
		# gravity
		if self.grav < 20:
			self.grav = self.grav + 0.95
		self.y = self.y + self.grav
		collisionsY()
		self.hitboxUpdate()
		
########################################################################








########################################################################

class WalkMoby(Thing):
	
	def __init__(self, x, y, sprList, spr, hitbox, path, direction, speed, alive):
		super().__init__(x, y, sprList, spr, hitbox)
		self.path = randint(1,2)
		self.speed = randint(3,10)
		self.direction = direction
		self.alive = True

		if self.path == 1:
			self.x = 638
			self.y = 1920
			self.direction = "right"
		elif self.path == 2:
			self.x = 3584
			self.y = 1920
			self.direction = "left"
		
		
	def pathing(self):
		if self.direction == "right":
			if self.x >= 3584:
				self.direction = "left"
			else:
				self.x += self.speed
		if self.direction == "left":
			if self.x <= 638:
				self.direction = "right"
			else:
				self.x -= self.speed
				
	def sprSet(self):
		if self.direction == "right":
			if dataCycle <= 5:
				self.spr = self.sprList["wr1"][0]
			else:
				self.spr = self.sprList["wr2"][0]
		if self.direction == "left":
			if dataCycle <= 5:
				self.spr = self.sprList["wl1"][0]
			else:
				self.spr = self.sprList["wl2"][0]
		
		#"sr" : [sprIbisStillR,sprPigeonStillRt],
		#"sl" : [sprIbisStillL,sprPigeonStillLt],
		#"jr" : [sprIbisFly2R,sprPigeonFly2Rt],
		#"jl" : [sprIbisFly2L,sprPigeonFly2Lt],
		#"fr" : [sprIbisFly1R,sprPigeonFly1Rt],
		#"fl" : [sprIbisFly1L,sprPigeonFly1Lt],
		#"wr1": [sprIbisWalk1R,sprPigeonWalk1Rt],
		#"wl1": [sprIbisWalk1L,sprPigeonWalk1Lt],
		#"wr2": [sprIbisWalk2R,sprPigeonWalk2Rt],
		#"wl2": [sprIbisWalk2L,sprPigeonWalk2Lt]
		
class FlyMoby(Thing):
	
	def __init__(self, x, y, sprList, spr, hitbox, path, direction, speed, amplitude, period, xoffset, yoffset, alive):
		super().__init__(x, y, sprList, spr, hitbox)
		self.path = randint(1,2)
		self.speed = randint(4,8)
		self.direction = direction
		# set flight path data (sinewave equation stuff)
		self.amplitude = 128*randint(1,3)
		self.period = 2*pi/(128*randint(5,10))
		self.xoffset = 128*randint(1,4)
		self.yoffset = 128*randint(-1,1)
		self.alive = True

		if self.path == 1:
			self.x = 0
			self.y = 1408
			self.direction = "right"
		elif self.path == 2:
			self.x = 4224
			self.y = 1408
			self.direction = "left"
		
	def pathing(self):
		# determine x movement
		if self.direction == "right":
			if self.x >= 4224:
				self.direction = "left"
			else:
				self.x += self.speed
		if self.direction == "left":
			if self.x <= 0:
				self.direction = "right"
			else:
				self.x -= self.speed
		# determine y movement
		self.y = self.amplitude * sin(self.period*(self.x-self.xoffset)) + 1408 + self.yoffset
		
	def sprSet(self):
		if self.direction == "right":
			if dataCycleOsc == 0:
				self.spr = self.sprList["fr"][0]
			else:
				self.spr = self.sprList["jr"][0]
		if self.direction == "left":
			if dataCycleOsc == 0:
				self.spr = self.sprList["fl"][0]
			else:
				self.spr = self.sprList["jl"][0]













class Trail:
	
	# establish the permanent coordinates
	def __init__(self,x,y,img):
		 self.x = player.x
		 self.y = player.y
		 self.img = player.spr
		 
		 for key in player.sprList.items():
			 if key[1][0] == player.spr:
				 self.img = key[1][1]
				 
			 		 
		 #self.img.set_alpha(100)
	
	# blit the trail
	def blit(self):
		#blit_alpha(wndwScreen, self.img, (self.x-dataScroll[0],self.y-dataScroll[1]), 128)
		wndwScreen.blit(self.img, (self.x-dataScroll[0],self.y-dataScroll[1]))














# 638, 1920
# 3584, 1920


















# window initialising
pygame.display.set_caption("foo")
pygame.display.set_icon(pygame.image.load("images/icon.png"))
wndwSize = (1536,896) # this leaves space for tiles as 12x7 (84 total)
wndwScreen = pygame.display.set_mode(wndwSize,0,32)



# function that renders a map
def rendermap(levelmap):
	global dataTileFrameCycle
	tileHitboxes.clear() # reset hitboxes
	liquidHitboxes.clear() # reset hitboxes
	tileY = 0
	for row in levelmap: # loop through each row in the level map
		tileX = 0
		for tile in row: # loop through a row in the level map
			if tile != "0": # checks if a tile exists at a given coordinate
				for i in tileDictList: # loops through the tile dictionaries keys
					# sorts the tile dictionary by index and looks for the needed tile
					if tile == i:
						if (tileDict[i])[1] != 3:
							dataTileDictKey = (tileDict[i])[0]
							if tile != "23":
								wndwScreen.blit(dataTileDictKey[int(dataTileFrameCycle/8-1)], (tileX*128-dataScroll[0],tileY*128-dataScroll[1])) # render the tile once found
							# render the tile's texture
							dataTileDictKey[int(dataTileFrameCycle/8-1)].set_colorkey((0,206,213))
							# checks if a hitbox needs to be created and creates it if so
							if (tileDict[i])[1] == 0:
								tileHitboxes.append(pygame.Rect(tileX*128,tileY*128,128,128))
							elif (tileDict[i])[1] == 1:
								liquidHitboxes.append(pygame.Rect(tileX*128,tileY*128,128,128))
			tileX = tileX + 1
		tileY = tileY + 1
	if dataTileFrameCycle == 33:
		dataTileFrameCycle = 0
	else:
		dataTileFrameCycle = dataTileFrameCycle + 1
		
def rendermapBackground(levelmap):
	global dataTileFrameCycle
	tileHitboxes.clear() # reset hitboxes
	liquidHitboxes.clear() # reset hitboxes
	tileY = 0
	for row in levelmap: # loop through each row in the level map
		tileX = 0
		for tile in row: # loop through a row in the level map
			if tile != "0": # checks if a tile exists at a given coordinate
				for i in tileDictList: # loops through the tile dictionaries keys
					# sorts the tile dictionary by index and looks for the needed tile
					if tile == i:
						if (tileDict[i])[1] == 3:
							dataTileDictKey = (tileDict[i])[0]
							if tile != "23":
								wndwScreen.blit(dataTileDictKey[int(dataTileFrameCycle/8-1)], (tileX*128-dataScroll[0],tileY*128-dataScroll[1])) # render the tile once found
							# render the tile's texture
							dataTileDictKey[int(dataTileFrameCycle/8-1)].set_colorkey((0,206,213))
							# checks if a hitbox needs to be created and creates it if so
							if (tileDict[i])[1] == 0:
								tileHitboxes.append(pygame.Rect(tileX*128,tileY*128,128,128))
							elif (tileDict[i])[1] == 1:
								liquidHitboxes.append(pygame.Rect(tileX*128,tileY*128,128,128))
			tileX = tileX + 1
		tileY = tileY + 1
	if dataTileFrameCycle == 33:
		dataTileFrameCycle = 0
	else:
		dataTileFrameCycle = dataTileFrameCycle + 1

def camera():
	if player.dash:
		dataScroll[0] += (player.hitbox.x-dataScroll[0]-704)/10
	else:
		dataScroll[0] += (player.hitbox.x-dataScroll[0]-704)/20
	dataScroll[1] += (player.hitbox.y-dataScroll[1]-448)/7

# corrects player's hitbox
def collisionsX():
	for tile in tileHitboxes:
		if player.movingRight:
			if player.hitbox.colliderect(tile):
				player.hitbox.right = tile.left
				player.x = player.hitbox.x
		if player.movingLeft:
			if player.hitbox.colliderect(tile):
				player.hitbox.left = tile.right
				player.x = player.hitbox.x

def collisionsY():
	for tile in tileHitboxes:
		if player.grav > 0:
			if player.hitbox.colliderect(tile):
				player.hitbox.bottom = tile.top
				player.y = player.hitbox.y
				player.grav = 0
		if player.grav < 0:
			if player.hitbox.colliderect(tile):
				if player.inLiquid == True:
					player.hitbox.top = tile.bottom
					player.y = player.hitbox.y
					player.grav = 1
				else:
					player.hitbox.top = tile.bottom
					player.y = player.hitbox.y

def collisionsE():
	
	global dataStatus
	global score
	global hp
	global gibisList
	global skibisList
	global iFrames
	global projectile
	global projectileExists
	
	for i in range(len(gibisList)):
		
		enemy = gibisList[i]
		
		if projectileExists == True:
			if projectile.hitbox.colliderect(enemy.hitbox) and enemy.alive == True:
				enemy.alive = False
				score += 1
				projectileExists = False
				del projectile
		
		if player.hitbox.colliderect(enemy.hitbox) and enemy.alive == True:
			if (enemy.y-player.y) > 80:
				player.grav = -12.5
				enemy.alive = False
				score += 1
			else:
				if iFrames == 0:
					if hp == 1:
						dataStatus = "death"
						updateScores()
					else:
						hp -= 1
						iFrames += 60
				
	
	for i in range(len(skibisList)):
		
		enemy = skibisList[i]
		if projectileExists == True:
			if projectile.hitbox.colliderect(enemy.hitbox) and enemy.alive == True:
				enemy.alive = False
				score += 2
				projectileExists = False
				del projectile
		
		if player.hitbox.colliderect(enemy.hitbox) and enemy.alive == True:
			if (enemy.y-player.y) > 80:
				player.grav = -12.5
				enemy.alive = False
				score += 2
			else:
				if iFrames == 0:
					if hp == 1:
						dataStatus = "death"
						updateScores()
					else:
						hp -= 1
						iFrames += 60
				



def collisionsL():
	pass



def playerTrail():
	if player.dash:
		dataTrailList.append(Trail(0,0,0))
	for q in range(len(dataTrailList)):
		dataTrailList[q].blit()

def charSelect():
	
	global scores
	
	global selectedChar
	charInfo = [
		[368,sprPigeonSelected,sprPigeonStillR],
		[568,sprThorntonSelected,sprThorntonStillR],
		[768,sprWaqaSelected,sprWaqaStillR],
		[968,sprWalrusSelected,sprWalrusStillR],
		[1168,sprDuckySelected,sprDuckyR]
	]
	
	for i in range(0,5):
		if i+1 == selectedChar:
			select = 1
			fntHighScore = pygame.font.SysFont("uroob",50)
			txtHighScore = fntHighScore.render(f"High Score: {scores[i]}",True,(255,255,255))
			rectHighScore = txtHighScore.get_rect()
			rectHighScore.center = (charInfo[i][0],560)
			wndwScreen.blit(txtHighScore,(rectHighScore.x,rectHighScore.y))
		else:
			select = 2
		rect = charInfo[i][select].get_rect()
		rect.center = (charInfo[i][0],408)
		wndwScreen.blit(charInfo[i][select],(rect.x,rect.y))
		
def updateScores():
	global scores
	global score
	
	if score > scores[selectedChar-1]:
		if selectedChar == 1:
			gameData["pigeonHS"] = score
		if selectedChar == 2:
			gameData["thorntonHS"] = score
		if selectedChar == 3:
			gameData["waqaHS"] = score
		if selectedChar == 4:
			gameData["walrusHS"] = score
		if selectedChar == 5:
			gameData["duckyHS"] = score
		
		scores = []
		for i in gameData:
			scores.append(gameData[i])
	

		



# cool fonts:
# keraleeyam, uroob, urwgothic, purisa, karumbi
scoreDisplay = f"Score: {score}"
fntScore = pygame.font.SysFont("uroob", 100)
txtScore = fntScore.render(scoreDisplay, True, (255,255,255))
rectScore = txtScore.get_rect()
rectScore.center = (180,100)

hpDisplay = f"HP: {hp}"
fntHP = pygame.font.SysFont("uroob",100)
txtHP = fntHP.render(hpDisplay, True, (255,255,255))
rectHP = txtHP.get_rect()
rectHP.center = (1356,100)




running = True
frames = 0
textFrames = 0

while running == True:
	
	if dataStatus == "menu":
		
		wndwScreen.fill((100,216,227))
		
		# menu title
		fntTitle = pygame.font.SysFont("purisa", 130)
		txtTitle = fntTitle.render("PIGEON vs IBIS 2", True, (255,255,255))
		rectTitle = txtTitle.get_rect()
		rectTitle.center = (768,150)
		wndwScreen.blit(txtTitle,(rectTitle.x,rectTitle.y))
		
		if textFrames > 30:
			fntStart = pygame.font.SysFont("uroob", 50)
			txtStart = fntStart.render("press SPACE to start", True, (255,255,255))
			rectStart = txtStart.get_rect()
			rectStart.center = (768,750)
			wndwScreen.blit(txtStart,(rectStart.x,rectStart.y))
		
		for event in pygame.event.get():
			
			# close window
			if event.type == QUIT:
				saveFile = open("pgn2_save.dat","wb")
				pickle.dump(gameData,saveFile)
				saveFile.close()
				pygame.quit()
				sys.exit()
		
		
			
			# key input retrieval
			if event.type == KEYDOWN:
				
				if event.key == K_SPACE:
					
					# initialise everything
					score = 0
					gibisList = []
					skibisList = []
					player = Player((128*11),(128*15),0,smaps[selectedChar-1], 1, False, False, 1, False, 3, False)
					gibisList.append(WalkMoby(3584,1920,smapIbis,1,0,1,"what",10,True))
					# skibisList.append(FlyMoby(3584,1920,smapIbis,1,0,1,"what",10,0,0,0,0))
					
					dataStatus = "menu" # intialise status
					dataScroll = [2176,0] # camera position
					dataCycle = 1 # is the frame odd or even?
					dataCycleOsc = 0
					dataTrailList = [] # this list stores the trail objects
					ticker = 0
					
					maxJumps = jumps[selectedChar-1]
					
					if selectedChar == 1:
						hp = 1
					if selectedChar == 2:
						hp = 2
					if selectedChar == 3:
						hp = 1
					if selectedChar == 4:
						hp = 3
					if selectedChar == 5:
						hp = 1
					
					dataStatus = "gameplay"
					
				if event.key == K_LEFT:
					if selectedChar != 1:
						selectedChar -= 1
				if event.key == K_RIGHT:
					if selectedChar != 5:
						selectedChar += 1
		
		charSelect()
		
		textFrames = textFrames + 1
		if textFrames > 60:
			textFrames = 0
				
		pygame.display.update() # update the window's display
		dataFrameClock.tick(60) # FPS regulation
		
	if dataStatus == "death":
		wndwScreen.fill((200,50,50))
		
		# you died
		fntDeath = pygame.font.SysFont("uroob", 100)
		txtDeath = fntDeath.render("You died!", True, (255,255,255))
		rectDeath = txtDeath.get_rect()
		rectDeath.center = (768,300)
		wndwScreen.blit(txtDeath,(rectDeath.x,rectDeath.y))
		
		# score: x
		fntEScore = pygame.font.SysFont("uroob",50)
		txtEScore = fntEScore.render(f"Score: {score}", True, (255,255,255))
		rectEScore = txtEScore.get_rect()
		rectEScore.center = (768,400)
		wndwScreen.blit(txtEScore,(rectEScore.x,rectEScore.y))
		
		# instruction
		fntExit = pygame.font.SysFont("uroob",50)
		txtExit = fntExit.render("(SPACE to exit)", True, (255,255,255))
		rectExit = txtExit.get_rect()
		rectExit.center = (768,500)
		wndwScreen.blit(txtExit,(rectExit.x,rectExit.y))
		
		for event in pygame.event.get():
			
			# close window
			if event.type == QUIT:
				saveFile = open("pgn2_save.dat","wb")
				pickle.dump(gameData,saveFile)
				saveFile.close()
				pygame.quit()
				sys.exit()
		
		
			
			# key input retrieval
			if event.type == KEYDOWN:
				
				if event.key == K_SPACE:
					
					dataStatus = "menu"
					
				
		pygame.display.update() # update the window's display
		dataFrameClock.tick(60) # FPS regulation

# gameplay loop
	if dataStatus == "gameplay":
		
		camera()
		
		# refresh the display
		if dataLevel == 1:
			wndwScreen.fill((100,216,227))
			
		# check number of alive gibis
		aliveCount = 0
		for i in gibisList:
			if i.alive == True:
				aliveCount += 1
		# spawn ibis
		if aliveCount > 9:
			if randint(1,95) == 1:
				skibisList.append(FlyMoby(3584,1920,smapIbis,1,0,1,"what",10,0,0,0,0,True))
		else:
			if randint(1,95) == 1:
				if randint(1,4) == 1:
					skibisList.append(FlyMoby(3584,1920,smapIbis,1,0,1,"what",10,0,0,0,0,True))
				else:
					gibisList.append(WalkMoby(3584,1920,smapIbis,1,0,1,"what",10,True))
					
		# update the display for on screen items (this also sets the hitboxes) 
		
		# ibis
		for i in range(len(gibisList)):
			if gibisList[i].alive == True:
				gibisList[i].pathing()
				gibisList[i].sprSet()
				gibisList[i].hitboxUpdate()
				gibisList[i].blit()
		for i in range(len(skibisList)):
			if skibisList[i].alive == True:
				skibisList[i].pathing()
				skibisList[i].sprSet()
				skibisList[i].hitboxUpdate()
				skibisList[i].blit()
		if projectileExists:
			projectile.hitboxUpdate()

		
		# background tiles
		if dataLevel == 1:
			rendermapBackground(mapLevel1)
		elif dataLevel == 2:
			rendermapBackground(mapLevel2)
		
		# player
		playerTrail()
		player.blit()
		
		
		# foreground objects
		if dataLevel == 1:
			rendermap(mapLevel1)
		elif dataLevel == 2:
			rendermap(mapLevel2) 
		
		
		# input retrieval
		for event in pygame.event.get():
			
			# close window
			if event.type == QUIT:
				saveFile = open("pgn2_save.dat","wb")
				pickle.dump(gameData,saveFile)
				saveFile.close()
				pygame.quit()
				sys.exit()
		
		
			
			# key input retrieval
			if event.type == KEYDOWN:
				
				# left and right movement
				if event.key == K_RIGHT or event.key == K_d:
					player.movingLeft = False
					player.movingRight = True
				if event.key == K_LEFT or event.key == K_a:
					player.movingRight = False
					player.movingLeft = True
				if event.key == K_SPACE:
					if len(dataTrailList) == 0 and player.dash == False and selectedChar != 3:
						player.dash = True
						ticker = 0
					if selectedChar == 3 and projectileCooldown == 0:
						projectile = Hat(player.x,player.y,0)
						projectileCooldown = 120
						projectileExists = True
				if event.key == K_UP or event.key == K_w:
					if player.jumps < maxJumps:
						player.grav = ySpeeds[selectedChar-1]
						animatedCheck = False
						player.jumps = player.jumps + 1
					
			if event.type == KEYUP:
				
				# left and right movement
				if event.key == K_RIGHT or event.key == K_d:
					player.movingRight = False
				if event.key == K_LEFT or event.key == K_a:
					player.movingLeft = False
		
		
		# movement
		collisionsE()
		collisionsL()
		player.movementX()
		collisionsX()
		player.movementY()
		collisionsY()
		player.sprSet()
		
		
		
		
		
		# stops player dash
		if ticker >= dashDuration[selectedChar-1]:
			player.dash = False
			if len(dataTrailList) > 0:
				dataTrailList.pop(0)
				
		# display the score
		txtScore = fntScore.render(f"Score: {score}", True, (255,255,255))
		wndwScreen.blit(txtScore,(rectScore.x,rectScore.y))
		
		txtHP = fntHP.render(f"HP: {hp}", True, (255,255,255))
		wndwScreen.blit(txtHP,(rectHP.x,rectHP.y))
		
		if projectileExists == True:
			projectile.motion()
			projectile.blit()
			
		if player.y > 3000:
			dataStatus = "death"
			updateScores()
		
		# update everything else for the display
		pygame.display.update() # update the window's display
		dataFrameClock.tick(60) # FPS regulation
		
		
		
		
		# odd or even cycle
		if dataCycle == 10:
			dataCycle = 1
			
			if dataCycleOsc == 0:
				dataCycleOsc = 1
			else:
				dataCycleOsc = 0
			
		else:
			dataCycle += 1
			
		if iFrames > 0:
			iFrames -= 1
		
		if projectileCooldown != 0:
			projectileCooldown -= 1
			
		ticker = ticker + 1
		
		
		
		# debug
		# test	

		
